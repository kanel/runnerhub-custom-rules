# Changed 2018-07-15

## Made cloaks only be able to take certain mods

> Changed cloak and designer cloak to only take Chemical Protection, Fire Resistance, Insulation, Nonconductivity, Radiation Shielding, and Thermal Dampening

## Added note to Internal Router

> Added “This piece of ‘ware can only provide wireless functionality for a character’s cyberware and devices with a direct physical connection, such as a smartgun system connected to a datajack via a cable. Wired Reflexes and Reaction Enhancers can be used together via an Internal Router.”

## Limited the amount of armor Lower arms/legs can have

> Lowered the amount to 2

## Removed the ability of Hands/Feet to have armor

# Changed 2018-07-17

## Changed Portable Chemical Injectors
## Added note to Shaman Tuxedo
## Made RCCs only accept certain programs as plugins
## Changed the disadvantage to Dolphin and Whale
## Removed the Path of Pariah
## Fixed the Realistic Feature (Drone) to only apply to certain drones
## Fixed the Ram Plate
> It now has 2 mod slot usage, 1500 nuyen, avail 8
## Fixed so only full armor pieces can get YNT Softweave and Gelpacks.
## Added workaround for RCC programs and Injector drugs.
## Changed how Shapeshifter init works
> Now stacks with augments, but can't go beyond 5d6