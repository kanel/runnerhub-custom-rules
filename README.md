# Runnerhub Custom Ruleset

## Introduction

> This project is for people to be able to add the houserule of the [runnerhub wiki](https://neosynth.net/index.php?n=Rules.HouseRules) for the chummer project! A special thanks to the [chummer5a](https://github.com/chummer5a/chummer5a) devs for all the help!

## Code Samples

> The project is mainly in XML, with changes being made to qualities. For example:

```XML
<chummer>
  <qualities>
    <quality>
      <name>Records on File</name>
      <notes>    
           - On character submission the player needs to provide a list of the corporations that have the files, 
             a long with what specific information the corporation holds on the character and why they have those records.
           - This list should be adequately explained in the character's background section on their character sheet, and 
              must be thematically appropriate to the background of the character. 
           - This quality is limited to 3 ranks (or 3 corporations) at character creation. 
      </notes>
      <limit>3</limit>
    </quality>
  </qualities>
</chummer>
```
> Would change the limit and the note for the "Records on file" quality.

> You can also select multiple objects with an `xpathfilter`

```XML
<chummer>
  <qualities>
    <quality xpathfilter="starts-with(name, 'Crystal')" amendoperation="append">
      <hide />
    </quality>
  </qualities>
</chummer>
```

> This would hide all qualities starting with "Crystal".

> There are also four `amendoperation` that can be used.
>> The amend system uses four operations: remove, replace, append, and recurse.
If no operation is specified, the default operation is recurse if the amending node has children elements, otherwise it's append if no target nodes were found, otherwise it's replace.
Recurse continues processing amend operations on children, replace will outright replace an entire node (sort of like override, but on a small scale), remove will remove a node if it exists, and append will add a node if it doesn't exist and concate text contents if it does exist.

## Installation

> Download the project and extract the Runnerhub Rules into your Chummer -> Customdata folder.

> Start chummer, and head into Tools -> Options -> Optional rules and enable the ruleset.

> Also, remember to set the karma cost for knowledge specializations to 4 under the Karma Cost tab under Options.

> Create a new character!

