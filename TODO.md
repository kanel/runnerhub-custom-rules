# To be added


## Shapeshifter qualities
> Check if impaired attribute can be changed to not apply to the lowest attribute of the charater.
> See if Ogre Stomach can be changed to only apply to medium and below (if it currently doesn't).

## Infected qualities
> Change some of the qualities to apply better, like Infected Armor being limited by your essence.

## Update so Gridlink is added to all vehicles, not some
> Check why some vehicle doesn't get the gridlink.

## Disable Cybertooth
> Can't find in chummer atm

## Disable Corpse Cadavers and Zombies
> Can't find in chummer atm

## Limit Cool Resolve to 2 + Initiate Grade
## Make Antenna Grill limited to orks/trolls/posers and require implanted decks/commlinks

## See if there's a way to include addiction stats for drugs

## Remove Hand Load
> Can't find in chummer atm

## Make Atomizer add an exotic skill
> Not possible atm

## Remove Astral Bond, Baobhan's Tears, Drain Away, HMHVV II Inihibitor to be hidden
> Can't find in chummer atm

## Double check that the new RCC programs work.
> Being implemented in chummer atm

## Make Null Wizard have summoning removed
> Not in chummer atm

## Make it so JoaT doesn't stack with CE/Ling/SohK/TSE
> Have to check with the developed/Look through the code